angular.module('ionicApp', ['ionic'])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('signin', {
      url: '/sign-in',
      templateUrl: 'templates/sign-in.html', //when sign in page is active, then "sign-in.html file will be displayed"
      controller: 'SignInCtrl'   //also "SignInCtrl" Controller will be called
    })
    .state('register', {
      url: '/register',
      templateUrl: 'templates/register.html', //when user is registering, "register.html" will be called and displayed
      controller: 'RegisterCtrl' //also RegisterCtrl will be called
    })
    .state('forgotpassword', {  //When user clicks forgot password
      url: '/forgot-password',
      templateUrl: 'templates/forgot-password.html', // "forgot-password.html" will be displayed
      controller: 'ForgotPwdCtrl' //also ForgotPwdCtrl will be called
    })
    .state('patients', {     //when radio button selected by user is "I am a Patient"
      url: '/patients',
      controller: 'PatientsCtrl', //This controller will be called
      templateUrl: 'templates/patients.html' // when user is selecting "I am a paitient", "patients.html" will be called and displayed
    })
    .state('patients.home', {  //when Patient logs in, Home screen appears, this is again a page or "state"
      url: '/home',
      views: {
        'home-tab': {
          templateUrl: 'templates/patients/home.html',  //when Patient logs in, "home.html" will be called
          controller: 'PatientHomeTabCtrl'   //also this controller will be called
        }
      }
    })
    .state('patients.profile', {  //when patient clicks "Profile" after loggin
      url: '/profile',
      views: {
        'profile-tab': {
          templateUrl: 'templates/patients/profile.html',  //The "profile.html" page will be displayed
          controller: 'ProfileTabCtrl'  //also this controller will come
        }
      }
    })
    .state('patients.edit', {  //when patient clicks "Profile" after loggin
      url: '/edit',
      views: {
        'edit-tab': {
          templateUrl: 'templates/patients/edit.html',  //The "profile.html" page will be displayed
          controller: 'EditCtrl'  //also this controller will come
        }
      }
    })
    .state('doctors', {   // when user selects "I am a Doctor" radio button
      url: '/doctors',
      controller: 'DoctorsCtrl',  // this controller will be called
      templateUrl: 'templates/doctors.html' // this doctor.html file will be displayed
    })
    .state('doctors.home', {   //once doctor has logged in, he will be directed to "Home screen"
      url: '/home',
      views: {
        'home-tab': {
          templateUrl: 'templates/doctors/home.html',  // home.html will be displayed
          controller: 'DoctorHomeTabCtrl' // DoctorHomeTabtrl will be called
        }
      }
    })
    .state('doctors.profile', {   // when the doctor selects "Profile"
      url: '/profile',
      views: {
        'profile-tab': {
          templateUrl: 'templates/doctors/profile.html', //profile.html will be displayed to the doctor
          controller: 'ProfileTabCtrl' // this controller will be called
        }
      }
    })
    .state('doctors.edit', {  //when patient clicks "Profile" after loggin
      url: '/edit',
      views: {
        'edit-tab': {
          templateUrl: 'templates/doctors/edit.html',  //The "profile.html" page will be displayed
          controller: 'EditCtrl'  //also this controller will come
        }
      }
    })

   $urlRouterProvider.otherwise('/sign-in');  // by default, this state will load
})

.factory('UserService', function() {
  return {
    load: function() {
      this.data = JSON.parse(window.localStorage.getItem('user'));
    },
    data: {}
  };
})

.controller('SignInCtrl', function($scope, UserService, $state, $window, $http, $ionicPopup) { //called when sign-in.html is displayed
  $scope.state = false;   // the button will be enabled
  $scope.user = { type: "patient" };  // default radio button status is "Patient"
  UserService.load();    //global data sharing
  if(UserService.data && UserService.data.token) {
    if(UserService.data.type == 'patient') {  // if the radio button's selected value is Patient,
      $state.go('patients.home');  // then navigate to "patients.home"
    } else {
      $state.go('doctors.home'); //else, go to docotr's home
    }
  }

  $scope.signIn = function(user) {  //when user clicks sign in button
    $scope.state = true; // the button is now disabled
    var url = "http://avasaram.ml/"+user.type+"/login";  // creating a url for webcall
    $http.post(url, user)  //making a web call to the server
    .then(function(data) { // gets the data from the server and is stored in "data" object
      $scope.state = false;  // now the button can be clicked or its enabled
      data.data.type = user.type;
      // localstorage is used to store some data locally in the browser.
      // Similar to cookies.
      window.localStorage.setItem('current', data.data.token);
      window.localStorage.setItem('user', JSON.stringify(data.data));
      $window.location.reload(); // reloads the page once login is successful
    }, function(err) {
      $scope.state = false;

      var alertPopup = $ionicPopup.alert({
        title: 'Error',
        template: 'Invalid email or password!'
      });
    });
  };

  $scope.register = function(){
    $state.go('register');
  }
})

.controller('RegisterCtrl', function($scope, $state, $http, $ionicPopup) { //called when the user is Registering
  $scope.state = false;  // button is clickable
  $scope.user = { type: "patient", gender: "M" }; // creating a user object with gender and type , default values, it will change when the user chooses

  $scope.register = function(user) {  // passing that user into function. This function is called when user clicks "Register"
  //button in the Sign-in Page (refer sign-in.html)
    $scope.state = true;
    var url = "http://avasaram.ml/"+user.type+"/register";  //web call URL to the server
    $http.post(url, user)  // making a web call to the server
    .then(function(data) { //when web server gives response, response goes to data, accesed by data. , as we access in Java
      $scope.state = false;  //button is clickable again

      var alertPopup = $ionicPopup.alert({
        title: 'Success',
        template: 'You have been successfully registered as a '+user.type+'!'  // Pop up comes that tells "Registration successful"
      });

      $state.go('signin');  // after successful registration, sign-in page is displayed
    }, function(err) {
      $scope.state = false;

      var alertPopup = $ionicPopup.alert({
        title: 'Error',
        template: 'Email or Phone Number already exists!'
      });
    });
  };
})

.controller('ForgotPwdCtrl', function($scope, $state, $http, $ionicPopup) {
  $scope.user = { type: 'patient' };

  $scope.reset = function(user) {
    $scope.state = true;
    var url = "http://avasaram.ml/"+$scope.user.type+"/forgot";  //web call URL to the server
    $http.post(url, user)
    .then(function(data) {
      $scope.state = false;

      var alertPopup = $ionicPopup.alert({
        title: 'Success',
        template: 'Email with new password was sent successfully.'
      });
      $state.go('/sign-in');
    }, function(err) {
      $scope.state = false;

      var alertPopup = $ionicPopup.alert({
        title: 'Error',
        template: 'User not found!'
      });
    });
  }
})

.controller('PatientHomeTabCtrl', function($scope, $state, $http, $ionicPopup) {    //// THIS IS FOR ?
  $scope.current = window.localStorage.getItem('current');
  if(!$scope.current) { $state.go('signin'); }
  var url = "http://avasaram.ml/doctor/search?access_token="+$scope.current;

  $http.get(url)
  .then(function(data) {
    $scope.state = false;

    $scope.doctors = data.data;
  });

  $scope.makeCall = function(id) {
    var url = "http://avasaram.ml/call/initiate?access_token="+$scope.current+"&id="+id;
    $http.get(url)
    .then(function(data) {
      var alertPopup = $ionicPopup.alert({
        title: 'Success',
        template: 'Your call to the doctor has been initiated. Please wait...'
      });

    }, function(err) {
      var alertPopup = $ionicPopup.alert({
        title: 'Error',
        template: err.data.error
      });
    });
  }
})

.controller('DoctorHomeTabCtrl', function($scope, UserService, $state, $http, $ionicPopup) {
  UserService.load();
  $scope.user = UserService.data;

  $scope.current = window.localStorage.getItem('current');
  if(!$scope.current) { $state.go('signin'); }

  $scope.transfer = function() {
    var myPopup = $ionicPopup.show({
      template: '<input type="password" ng-model="data.wifi">',
      title: 'Enter Account Number',
      scope: $scope,
      buttons: [
        { text: 'Cancel' },
        {
          text: '<b>Transfer</b>',
          type: 'button-positive',
          onTap: function(e) {
          }
        }
      ]
    });
  }

})

.controller('ProfileTabCtrl', function($scope, UserService, $state, $http, $ionicPopover, $ionicPopup) { // THIS IS FOR ?
  UserService.load();
  $scope.user = UserService.data;
  $scope.transaction = { card: { object: "card" } };

  $scope.openPopover = function($event) {
    $ionicPopover.fromTemplateUrl('templates/payment-modal.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
      $scope.popover.show($event);
    });
  };

  $scope.closePopover = function() {
    $scope.popover.hide();
  };

  $scope.edit = function() {
    $state.go('patients.edit', {}, { reload: true });
  }

  $scope.pay = function() {
    $scope.current = window.localStorage.getItem('current');
    var url = "http://avasaram.ml/transaction/loadCash?access_token="+$scope.current;  //web call URL to the server
    var transaction = $scope.transaction;
    $http.post(url, transaction)
    .then(function(data) {
      UserService.data.callPoints = parseInt(UserService.data.callPoints) + parseInt($scope.transaction.amount);
      window.localStorage.setItem('user', JSON.stringify(UserService.data));
      $ionicPopup.alert({
        title: 'Success',
        template: data.data.message
      });
      $scope.popover.remove();
    }, function(err) {
      $ionicPopup.alert({
        title: 'Error',
        template: err.data.message
      });
      $scope.popover.remove();
    });
  }
})

.controller('PatientsCtrl', function($scope, $state) { // When patient clicks Profile, this controller is called
  $scope.profile = function() {
    $state.go('patients.profile', {}, { reload: true }); // THIS IS FOR ?
  }

  $scope.edit = function() {
    $state.go('patients.edit', {}, { reload: true });
  }

  $scope.signout = function () {
    window.localStorage.removeItem('current'); // THIS IS FOR ?
    window.localStorage.removeItem('user'); // THIS IS FOR ?
    $state.go('signin');
  }
})

.controller('DoctorsCtrl', function($scope, $state) { // When doctor clicks Profile, this controller is called
  $scope.profile = function() {
    $state.go('doctors.profile', {}, { reload: true });
  }

  $scope.edit = function() {
    $state.go('doctors.edit', {}, { reload: true });
  }

  $scope.signout = function () {  // THIS IS FOR ?
    window.localStorage.removeItem('current');
    window.localStorage.removeItem('user');
    $state.go('signin');
  }
})

.controller('EditCtrl', function($scope, UserService, $state, $http, $window, $ionicPopup) {
  UserService.load();
  $scope.data = UserService.data;
  $scope.current = window.localStorage.getItem('current');
  if(!$scope.current) { $state.go('signin'); }

  $scope.user = {};
  $scope.user.name = $scope.data.name;
  $scope.user.phoneNumber = $scope.data.phoneNumber;
  $scope.user.gender = $scope.data.gender;
  $scope.user.type = $scope.data.type;
  $scope.user.rate = $scope.data.rate;

  $scope.update = function (user) {
    var url = "http://avasaram.ml/"+$scope.user.type+"/edit?access_token="+$scope.current;  //web call URL to the server
    $http.post(url, user)
    .then(function(data) {
      $scope.state = false;

      data.data.type = user.type;
      window.localStorage.setItem('user', JSON.stringify(data.data));

      var alertPopup = $ionicPopup.alert({
        title: 'Success',
        template: 'Profile updated successfully.'
      });

      alertPopup.then(function() {
        $window.location.reload();
      });
    }, function(err) {
      $scope.state = false;

      var alertPopup = $ionicPopup.alert({
        title: 'Error',
        template: 'Error while updating profile!'
      });
    });
  }

  $scope.cancel = function() {
    $state.go('patients.profile', {}, { reload: true });
  }
});
